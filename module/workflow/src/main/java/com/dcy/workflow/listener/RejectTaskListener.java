package com.dcy.workflow.listener;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.dcy.common.constant.TaskConstants;
import com.dcy.common.enums.TaskTypeEnum;
import com.dcy.common.service.ActCompleteTaskService;
import lombok.extern.slf4j.Slf4j;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.delegate.TaskListener;

import java.util.List;
import java.util.Optional;

/**
 * @Author：dcy
 * @Description: 驳回任务监听器（用于每个审批节点）
 * @Date: 2021/6/15 9:25
 */
@Slf4j
public class RejectTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("驳回任务监听器");
        // ====================获取基本信息========================
        final String status = delegateTask.getVariable(TaskConstants.TASK_STATUS, String.class);
        if (StrUtil.isNotBlank(status) && TaskConstants.TASK_REJECT.equals(status)) {
            final String businessKey = delegateTask.getVariable(TaskConstants.TASK_BUSINESS_KEY, String.class);
            log.info("businessKey:{}", businessKey);
            final List<String> list = StrUtil.split(businessKey, ":");
            // 获取业务类型
            final String businessType = CollUtil.getFirst(list);
            // 获取主键id
            final String id = CollUtil.getLast(list);
            if (StrUtil.isNotBlank(businessType) && StrUtil.isNotBlank(id)) {
                Optional.ofNullable(TaskTypeEnum.getByCode(businessType)).flatMap(taskTypeEnum -> Optional.ofNullable(SpringUtil.getBean(taskTypeEnum.serviceName, ActCompleteTaskService.class))).ifPresent(actCompleteTaskService -> {
                    // 驳回状态
                    actCompleteTaskService.updateRejectStatusById(id);
                });
            }
        }
    }

}