package com.dcy.workflow.listener;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.dcy.common.constant.TaskConstants;
import com.dcy.common.enums.TaskTypeEnum;
import com.dcy.common.service.ActCompleteTaskService;
import lombok.extern.slf4j.Slf4j;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.delegate.TaskListener;

import java.util.List;
import java.util.Optional;

/**
 * @Author：dcy
 * @Description: 完成用户任务监听器（用于最后一个审批节点）
 * @Date: 2021/6/8 8:28
 */
@Slf4j
public class CompleteTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("完成用户任务监听器");
        // ====================获取基本信息========================
        final String businessKey = delegateTask.getVariable(TaskConstants.TASK_BUSINESS_KEY, String.class);
        final String status = delegateTask.getVariable(TaskConstants.TASK_STATUS, String.class);
        log.info("businessKey:{}", businessKey);
        final List<String> list = StrUtil.split(businessKey, ":");
        // 获取业务类型
        final String businessType = CollUtil.getFirst(list);
        // 获取主键id
        final String id = CollUtil.getLast(list);
        if (StrUtil.isNotBlank(businessType) && StrUtil.isNotBlank(id) && StrUtil.isNotBlank(status)) {
            Optional.ofNullable(TaskTypeEnum.getByCode(businessType)).flatMap(taskTypeEnum -> Optional.ofNullable(SpringUtil.getBean(taskTypeEnum.serviceName, ActCompleteTaskService.class))).ifPresent(actCompleteTaskService -> {
                switch (status) {
                    case TaskConstants.TASK_SUCCESS:
                        // 通过状态
                        actCompleteTaskService.updateCompleteStatusById(id);
                        break;
                    case TaskConstants.TASK_REJECT:
                        // 驳回状态
                        actCompleteTaskService.updateRejectStatusById(id);
                        break;
                    default:
                }
            });
        }
    }

}
