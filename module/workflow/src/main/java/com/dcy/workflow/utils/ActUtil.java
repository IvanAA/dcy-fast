package com.dcy.workflow.utils;

import cn.hutool.core.util.StrUtil;
import com.dcy.common.enums.TaskTypeEnum;
import com.dcy.workflow.listener.CompleteTaskListener;
import com.dcy.workflow.listener.RejectTaskListener;
import com.dcy.workflow.listener.TaskCandidateListener;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.dcy.common.constant.TaskConstants.*;

/**
 * @author dcy
 * @description 工作流工具类
 * @createTime 2022/8/2 9:18
 */
public class ActUtil {

    /**
     * 获取公共的流程变量
     *
     * @param businessKey
     * @param userId
     * @return
     */
    public static Map<String, Object> getCommonVar(String businessKey, String userId) {
        return ImmutableMap.of(
                TASK_BUSINESS_KEY, businessKey,
                TASK_USER_ID, userId,
                TASK_COMPLETE_LISTENER, new CompleteTaskListener(),
                TASK_REJECT_LISTENER, new RejectTaskListener(),
                TASK_CANDIDATE_LISTENER, new TaskCandidateListener()
        );
    }

    /**
     * 获取业务key
     *
     * @param businessId
     * @param taskTypeEnum
     * @return
     */
    public static String getBusinessKey(String businessId, TaskTypeEnum taskTypeEnum) {
        // 拼装 业务key
        return StrUtil.builder()
                .append(taskTypeEnum.businessType)
                .append(":")
                .append(businessId).toString();
    }
}
