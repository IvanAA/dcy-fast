package com.dcy.common.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;

/**
 * @Author dcy
 * @Description: 工作流枚举
 * @Date: 2021/12/20
 */
@AllArgsConstructor
public enum TaskTypeEnum {

    LEAVE("leave", "审批流程", "leaveService"),


    ;


    /**
     * 业务类型
     */
    public final String businessType;

    /**
     * 描述
     */
    public final String info;

    /**
     * service bean名称
     */
    public final String serviceName;


    public static TaskTypeEnum getByCode(String code) {
        if (StrUtil.isBlank(code)) {
            return null;
        }
        for (TaskTypeEnum enums : TaskTypeEnum.values()) {
            if (enums.businessType.equals(code)) {
                return enums;
            }
        }
        return null;
    }
}
