package com.dcy.common.constant;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/8/24 10:57
 */
public interface TaskConstants {

    /**
     * 工作流相关
     */
    String TASK_STATUS = "status";
    String TASK_USER_ID = "userId";
    String TASK_BUSINESS_KEY = "businessKey";
    String TASK_COMMENT = "comment";
    String TASK_SUCCESS = "success";
    String TASK_REJECT = "reject";

    /**
     * 完成任务监听器流程变量key
     */
    String TASK_COMPLETE_LISTENER = "completeListener";
    /**
     * 驳回任务监听器流程变量key
     */
    String TASK_REJECT_LISTENER = "rejectListener";
    /**
     * 审批任务监听器流程变量key
     */
    String TASK_CANDIDATE_LISTENER = "candidateListener";
    /**
     * 过滤审批节点
     */
    String TASK_CANDIDATE_LISTENER_EXP = "${candidateListener}";

}
